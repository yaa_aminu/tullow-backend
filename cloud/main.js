Parse.Cloud.define("contactDriver", (req, res) => {
  if (!req.user) {
    return res.error("UnAuthorized");
  }

  let query = new Parse.Query(Parse.Installation);

  query.equalTo("truckId", req.params.truckId);

  Parse.Push.send(
    {
      where: query,
      priority: "high",
      data: {
        message: {
          action: "tripRequest",
          client: {
            userId: req.user.id,
            name: req.user.get("name"),
            dp: req.user.get("dp")
          },
          data: req.params.data
        }
      }
    },
    {
      useMasterKey: true,
      success: function() {
        // Push sent!
        res.success("sent");
      },
      error: function(error) {
        // There was a problem :(
        res.error(error);
      }
    }
  );
  console.log("sent push");
});

Parse.Cloud.define("reportLocation", (req, res) => {
  if (!req.user) return res.error("UnAuthorized");

  makeParseQuery("truck")
    .get(req.user.get("truckId"))
    .then(truck => {
      truck.set("latLang", new Parse.GeoPoint(req.params.lat, req.params.lng));
      return truck.save();
    })
    .then(truck => {
      if (!req.params.tripId) {
        return res.success();
      }
      return makeParseQuery("trip").get(req.params.tripId);
    })
    .then(trip => {
      if (trip) {
        trip.add(
          "trail",
          JSON.stringify({
            lat: req.params.lat,
            lng: req.params.lng,
            timeReported: req.params.timeReported,
            serverTimestamp: Date.now()
          })
        );
        return trip.save();
      }
    })
    .then(resolved => res.success())
    .catch(error => {
      return res.error(error);
    });
});

Parse.Cloud.define("rejectTripRequest", (req, res) => {
  if (!req.user) {
    return res.error("UnAuthorized");
  }

  let query = new Parse.Query(Parse.Installation);
  query.equalTo("userId", req.params.userId);

  Parse.Push
    .send(
      {
        where: query,
        priority: "high",
        data: {
          message: {
            action: "rejectTrip",
            driverId: req.user.id
          }
        }
      },
      { useMasterKey: true }
    )
    .then(() => {
      console.log("sent successfully");
    })
    .catch(err => {
      console.log(err);
    });
  res.success("sent successfully");
});

Parse.Cloud.define("startTrip", (req, res) => {
  if (!req.user) {
    return res.error("UnAuthorized");
  }

  makeParseQuery("truck")
    .get(req.user.get("truckId"))
    .then(truck => {
      let trip = new (Parse.Object.extend("trip"))();
      trip.set("truckId", req.params.truckId);
      trip.set("hiredBy", req.params.hiredBy);
      trip.set("startLocation", req.params.startLocation);
      trip.set("endLocation", req.params.endLocation);
      trip.set("initialTruckLocation", req.params.initialTruckLocation);
      trip.set("driverId", req.user.id);
      trip.set("endTime", -1);
      trip.set('tripName',req.params.tripName)
      trip.set('driverName',req.user.get('name'))
      trip.set('driverDp',req.user.get('dp'))
      trip.set('vehicleNo',truck.get('regNo'))
      truck.set("available", false);
      return Promise.all([trip.save(), truck.save()]);
    })
    .then(objects => {
      let trip = objects[0]; //se the promise above

      let query = new Parse.Query(Parse.Installation);
      query.equalTo("userId", trip.get("hiredBy"));

      res.success(trip); //tell the  caller

      //TODO schedule this as a job
      return Parse.Push.send(
        {
          where: query,
          priority: "high",
          data: {
            message: {
              action: "acceptTrip",
              tripId: trip.id,
              truckId: req.params.truckId,
              driverName: req.user.get("name"),
              driverId: req.user.id
            }
          }
        },
        { useMasterKey: true }
      );
    })
    .then(() => {
      console.log("trip sent successfully");
    })
    .catch(err => {
      res.error(err);
    });
});

Parse.Cloud.define("endTrip", (req, res) => {
  if (!req.user) {
    return res.error("UnAuthorized");
  }

  let trip;
  makeParseQuery("trip")
    .get(req.params.tripId)
    .then(obj => {
      trip = obj;
      let geoPoint = { lat: req.params.lat, long: req.params.long };
      trip.set("endLocation", JSON.stringify(geoPoint));
      trip.set("endTime", Date.now());
      return makeParseQuery("truck").get(trip.get("truckId"));
    })
    .then(truck => {
      if (req.user.id != truck.get("ownerId")) {
        throw new Error("UnAuthorized");
      }
      truck.set("available", true);
      return Promise.all([trip.save(), truck.save()]);
    })
    .then(objs => {
      res.success(objs[0]);

      let query = new Parse.Query(Parse.Installation);
      query.equalTo("userId", objs[0].get("hiredBy"));

      sendPush("endTrip", query, objs[0]).then(
        () => {
          console.log("push sent successfull");
        },
        err => {
          console.log(`encountered ${err} while sending push`);
        }
      );
    })
    .catch(err => {
      res.error(err);
    });
});

Parse.Cloud.afterSave("message", req => {
  let object = req.object;
  let query = new Parse.Query(Parse.Installation);
  query.equalTo("userId", object.get("recipientId"));

  return Parse.Push.send(
    {
      where: query,
      priority: "high",
      data: {
        message: {
          action: "message",
          senderId: object.get("senderId"),
          senderName: object.get("senderName"),
          messageId: object.get("messageId"),
          text: object.get("text"),
          remoteUrl: object.get("remoteUrl"),
          type: object.get("type"),
          dateComposed: object.createdAt.getTime()
        }
      }
    },
    { useMasterKey: true }
  );
});

function sendPush(action, query, obj) {
  let payload = {
    action: action,
    trip: {
      startTime: obj.createdAt.getTime(),
      truckId: obj.get("truckId"),
      tripId: obj.id,
      driverId: obj.get("driverId")
    }
  };

  return Parse.Push.send(
    {
      where: query,
      priority: "high",
      data: {
        message: payload
      }
    },
    { useMasterKey: true }
  );
}

function makeParseQuery(className) {
  var obj = Parse.Object.extend(className);
  return new Parse.Query(obj);
}
